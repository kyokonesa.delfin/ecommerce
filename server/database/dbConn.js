require('dotenv').config();

const mongoose = require('mongoose');


const connectDB = async () => {
	try {
		await mongoose.connect(process.env.DATABASE, {
			useNewUrlParser: true,
			useUnifiedTopology: true
		});

		console.log("Database successfully connected.");
	} catch (error) {
		console.log(error);
		process.exit(1);
	}
}

	
module.exports = connectDB;

