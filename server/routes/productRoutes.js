const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require("../middleware/auth");
const author = require("../middleware/authorize");




router.post('/addProduct', author.isAdmin, (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
})

router.get('/allProducts', author.isAdmin, (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})




module.exports = router;