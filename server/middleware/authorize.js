const jwt = require('jsonwebtoken');


module.exports.isAdmin = (req, res, next) => {

	let token = req.headers.authorization;

 	if (typeof token !== "undefined") {

 		token = token.slice(7, token.length);

 		return jwt.verify(token, process.env.SECRET_KEYWORD, (error, data) => {
 			if (error) {
 				return res.send({auth: "failed"});
 			} else {
 				if (jwt.decode(token, {complete: true}).payload.isAdmin) {
 					next();
 				} else {
 					return res.send({auth: "failed"});
 				}
 			}
 		})

 	} else {
 		return res.send({auth: "failed"});
 	}
}