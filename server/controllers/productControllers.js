const Product = require('../models/productSchema');


module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		id: reqBody.id,
		title: reqBody.title,
		price: reqBody.price,
		desc: reqBody.desc,
		image: reqBody.image,
		stocks: reqBody.stocks
	});


	return Product.find({
			id: reqBody.id,
			title: reqBody.title,
			price: reqBody.price,
			desc: reqBody.desc,
			image: reqBody.image,
			stocks: reqBody.stocks		
		}).then(result => {
			console.log(result)
			if (result.length > 0) {
				return false
			} else {
					return newProduct.save().then((user, error) => {
						if(error) {
							return {reg: 'Error'}
						} else {
							return true
						}
					})
				}
			})
		}


module.exports.getAllProducts = () => {

		return Product.find().then(result => {
			if (result.length <= 0) {
				return false
			} else {
				return result
			}
		})
	}