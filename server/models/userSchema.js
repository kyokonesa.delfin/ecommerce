const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');


// User Schema Or Document Structure
const userSchema = new mongoose.Schema({
	fname : {
		type: String,
		required : true,
		unique : true
	},
	lname : {
		type: String,
		required : true,
		unique : true
	},
	email : {
		type: String,
		required : true,
		unique : true
	},
	password :  {
		type : String,
		required : true
	},
	isAdmin: {

		type: Boolean,
		default: false
	},
	
	tokens : [
		{
			token : {
				type : String,
				required : true
			}
		}
	]
})


//Hashing Password to Secure
userSchema.pre('save', async function(next){
	if(this.isModified('password')){
		this.password = bcryptjs.hashSync(this.password, 10);
	}
	next();
})

//Generate Tokens to Verify User 
userSchema.methods.generateToken = async function(){
	try{
		let generatedToken = jwt.sign({_id : this._id}, process.env.SECRET_KEY);
		this.tokens = this.tokens.concat({token : generatedToken});
		await this.save();
		return generatedToken;
	} catch (error) {
		console.log(error)
	}
}

//  Create Model
const Users = new mongoose.model("USER", userSchema);

module.exports = Users;